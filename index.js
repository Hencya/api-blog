const express = require('express');

const app = express();

const port = 3000;
const host = 'localhost';

app.use(() => {
  console.log(`Server berjalan di http:://${host}:${port}`);
});

app.listen(port);
